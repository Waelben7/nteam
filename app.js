'use strict';

var express = require('express'),
    env = process.env.NODE_ENV || 'development',
    config = require('./config/config.js')[env],
    http = require('http'),
    server;


var app = express();

require('./config/express')(app, config);
require('./config/routes')(app);


server = http.createServer();
server.on('request', app);
server.listen(config.port, function () {

    console.log('The environment is : ', env);
    console.log("listening on port " + config.port);

});

module.exports = app;