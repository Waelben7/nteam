app.controller('courseController', function ($scope, $rootScope, CourseService) {

    CourseService.getCourses($rootScope.token)
      .then(function (object) {
          $scope.courses = object.data.courses;
      })
      .catch(function(err){
          console.log(err);
      })
})