app.factory('CourseService', function ($http) {

    var host = "http://localhost:3000/api/course/";

    return {

        getCourses: function (token) {

            return $http.get(host + "getAllCoursesByDivision", {
                headers: {
                    'token': token
                }
            });
        }
    }
})