app.controller('LoginController', function ($scope, $rootScope, $window, LoginService) {

    $scope.login = function () {

        LoginService.login($scope.email, $scope.password)
            .then(function (object) {

                if (object.data.success) {
                    $window.location.href = '/';
                } else {
                    console.log(object.data);
                }

            }).catch(function (err) {

                console.log(err);
            })
    }
})