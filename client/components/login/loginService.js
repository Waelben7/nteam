app.factory('LoginService', function ($http) {

    var host = "http://localhost:3000/api/";

    return {

        login: function (email, password) {
    
                return $http.post(host + "login", {
                    email: email,
                    password: password
                }); 
            }
    }
})