app.controller('registerController', function ($scope, $rootScope, RegisterService) {
    
    RegisterService.getDivisions()
        .then(function (object) {
            $scope.divisions = object.data.divisions;
        })
        .catch(function (err) {
            console.log(err);
        })

    $scope.register = function () {

        RegisterService.register($scope.name, $scope.email, $scope.password, $scope.age, $scope.division.id)
            .then(function (object) {

                $rootScope.username = $scope.name;
                $scope.name = '';
                $scope.email = '';
                $scope.password = '';
                $scope.age = '';
                $scope.divisionId = '';

                $rootScope.token = object.data.token;
                //console.log(object.data);
            }).catch(function (err) {

                console.log(err);
            })
    }

})