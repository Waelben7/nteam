app.factory('RegisterService', function ($http) {

    var host = "http://localhost:3000/api/";

    return {

        register: function (name, email, password, age, divisionId) {

            return $http.post(host + "register", {
                name: name,
                email: email,
                password: password,
                age: age,
                divisionId: divisionId
            }); // +params  //body , {}
        },
        
        getDivisions: function () {
            return $http.get(host + "division/allDivisions");
        },
    }
});