var app = angular.module("mainApp", ["ngRoute"]);

app.config(function ($routeProvider) {

    $routeProvider
        .when('/register', {
            templateUrl: 'components/register/register.html',
            controller: 'registerController'
        })
        .when('/login', {
            templateUrl: 'components/login/login.html',
            controller: 'LoginController'
        });
})