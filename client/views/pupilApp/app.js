var app = angular.module("pupilApp", ["ngRoute"]);

app.config(function ($routeProvider) {

    $routeProvider
        .when('/course', {
            // templateUrl: 'components/register/register.html',
            // controller: 'registerController'
        })
})


app.controller('pupilController', function ($scope, $rootScope, $http, $window) {

    $scope.logout = function () {

        $http.post("http://localhost:3000/api/logout")
            .then(function (object) {

                if (object.data.success) {
                    $window.location.href = '/';
                } else {
                    console.log(object.data);
                }

            }).catch(function (err) {
                console.log(err);
            })
    }
})