'use strict';

var db = require('../database/models');
var crypto = require('crypto');

module.exports = {

    generateTokenForPupil: function (callback) {

        var newToken = crypto.createHash('sha256').update(crypto.randomBytes(48).toString('hex')).digest('hex');

        db.User.create({

            email: 'pupil@test.tn',
            password: '12345678',
            name: 'pupil',
            token: newToken

        }).then(function (user) {

            db.Pupil.create({
                age: '19',
                point: 0,
                UserId: user.id,
                DivisionId: 1
            }).then(function (pupil) {

                return callback({
                    token: newToken,
                    pupilId: pupil.id,
                    userId: user.id,
                    divisionId: pupil.DivisionId
                })

            }).catch(function (err) {
                return callback({
                    token: '-1'
                })
            });

        }).catch(function (err) {
            return callback({
                token: '-1'
            })
        });
    },

    generateTokenForAdmin: function (callback) {

        var newToken = crypto.createHash('sha256').update(crypto.randomBytes(48).toString('hex')).digest('hex');

        db.User.create({

            email: 'admin@test.tn',
            password: '12345678',
            name: 'admin',
            token: newToken

        }).then(function (user) {

            db.Admin.create({
                UserId: user.id
            }).then(function (admin) {
                
                return callback({
                    token: newToken
                })

            }).catch(function (err) {
                return callback({
                    token: '-1'
                })
            });

        }).catch(function (err) {
            return callback({
                token: '-1'
            })
        });
    },

    destroyTokens : function (pupilToken, adminToken, callback) {

        db.User.findOne({
            where: {
                token: adminToken
            }
        }).then(function (adminUser) {
           
            db.User.findOne({
                where: {
                    token: pupilToken
                }
            }).then(function (pupilUser) {
              
                db.Pupil.destroy({
                    where: {
                        UserId: pupilUser.id
                    }
                }).then(function () {
                    
                    db.Admin.destroy({
                        where: {
                            UserId: adminUser.id
                        }
                    }).then(function () {
                        
                        db.User.destroy({
                            where: {
                                id: {
                                    $in: [pupilUser.id, adminUser.id]
                                }
                            }
                        }).then(function () {
                            
                            return callback({
                                success: true
                            })

                        }).catch(function (err) {
                            return callback({
                                success: false,
                                msg: err.toString()
                            })
                        });

                    }).catch(function (err) {
                        return callback({
                            success: false,
                            msg: err.toString()
                        })
                    });

                }).catch(function (err) {
                    return callback({
                        success: false,
                        msg: err.toString()
                    })
                });

            }).catch(function (err) {
                return callback({
                    success: false,
                    msg: err.toString()
                })
            });

        }).catch(function (err) {
            return callback({
                success: false,
                msg: err.toString()
            })
        });
    }
}