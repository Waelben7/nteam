module.exports = {
    development: {
        port: process.env.PORT || 3000
    },
    staging: {
        port: process.env.PORT || 3000
    },
    production: {
        port: process.env.PORT || 3000
    }
}