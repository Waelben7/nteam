var bodyParser = require('body-parser')
var express = require('express')
var path = require('path')

module.exports = function (app, config) {

    app.use(bodyParser.json({
        limit: "50mb" //max requete
    }));
    app.use(bodyParser.urlencoded({
        extended: false 
    }));

    var rootPath = path.normalize(__dirname + './../');
    app.use(express.static(path.join(rootPath, 'client')));

}