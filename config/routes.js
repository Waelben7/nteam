'use strict';

var session = require('express-session')
var auth = require('../controllers/auth/auth')();
var division = require('../controllers/division/division')();
var pupil = require('../controllers/pupil/pupil')();
var course = require('../controllers/course/course')();
var views = require('../controllers/views/views')();

module.exports = function (app) {

    app.use(session({
        secret: 'NTeam',
        resave: false,
        saveUninitialized: true
    }))

    app.use('/', views);
    app.use('/api', auth);
    app.use('/api/division', division);
    app.use('/api/pupil', pupil);
    app.use('/api/course', course);

}