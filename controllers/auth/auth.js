'use strict';

var express = require('express');
var router = express.Router();
var authController = require("./authController.js");

module.exports = function(){
    
    router.post('/register',authController.register);

    router.post('/login',authController.login);
    
    router.post('/logout',authController.logout);

    return router;
};