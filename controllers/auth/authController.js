'use strict';
var crypto = require('crypto');

var db = require('../../database/models');

exports.register = function (req, res) {
    if (req.body.email && req.body.password && req.body.name && req.body.age && req.body.divisionId) {
        var newToken = crypto.createHash('sha256').update(crypto.randomBytes(48).toString('hex')).digest('hex');
        db.User.create({

            email: req.body.email,
            password: req.body.password,
            name: req.body.name,
            token: newToken
        }).then(function (user) {

            db.Pupil.create({
                age: req.body.age,
                point: 0,
                UserId: user.id,
                DivisionId: req.body.divisionId
            }).then(function (pupil) {
                return res.json({
                    success: true,
                    token: user.token,
                    // pupilId : pupil.id,
                    msg: 'Pupil registered !!'
                });
            }).catch(function (err) {
                return res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        }).catch(function (err) {

            return res.json({
                success: false,
                msg: err.toString()
            });

        });
    } else {

        return res.json({
            success: false,
            msg: 'Email,password,name,division Id and age are obligatory !!'
        });
    }
}

exports.login = function (req, res) {

    // console.log('-------------> ', req.session.token)

    if (req.body.email && req.body.password) {
        var newToken = crypto.createHash('sha256').update(crypto.randomBytes(48).toString('hex')).digest('hex');
        db.User.findOne({
            where: {
                email: req.body.email.toLowerCase()
            }
        }).then(function (user) {


            if (user) {

                if (req.body.password == user.password) {

                    user.update({
                        token: newToken
                    }).then(function () {

                        req.session.token = newToken

                        console.log('-------------> I am lenna ! ', req.session.token)

                        return res.json({
                            success: true
                        });

                    }).catch(function (err) {
                        return res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });

                } else {
                    return res.json({
                        success: false,
                        msg: "password is incorrect !!"
                    });
                }

            } else {
                return res.json({
                    success: false,
                    msg: "Wrong email !!"
                });
            }

        }).catch(function (err) {
            return res.json({
                success: false,
                msg: err.toString()
            });
        });
    } else {
        return res.json({
            success: false,
            msg: "Email and password are obligatory !!"
        });
    }
}

exports.logout = function (req, res) {

    if (req.session.token) {

        db.User.findOne({
            where: {
                token: req.session.token
            }
        }).then(function (user) {

            if (user) {
                
                user.update({
                    token: null
                }).then(function () {

                    req.session.token = null

                    return res.json({
                        success: true
                    });

                }).catch(function (err) {
                    return res.json({
                        success: false,
                        msg: err.toString()
                    });
                });

            } else {
                return res.json({
                    success: false,
                    msg: "You are not authorised !!"
                });
            }

        }).catch(function (err) {
            return res.json({
                success: false,
                msg: err.toString()
            });
        });

    } else {
        return res.json({
            success: true
        });
    }

}