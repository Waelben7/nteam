var express = require('express');
var router = express.Router();
var courseController = require("./courseController.js");
var mdw = require("./../../middlewares/middleware.js");

module.exports = function () {

    router.post("/addCourse", mdw.isAdmin, courseController.addCourse);

    router.get("/getAllCourses", mdw.isAdmin, courseController.getAllCourses);

    router.get("/getCourseByState/:state", mdw.isAdmin, courseController.getCourseByState);

    router.get("/getCoursesByDivision/:division", mdw.isAdmin, courseController.getCoursesByDivision);

    router.get("/getCourseByDivisionState/:division/:state", mdw.isAdmin, courseController.getCourseByDivisionState);

    router.put("/updateCourse", mdw.isAdmin, courseController.updateCourse);

    router.patch("/updateCourseState", mdw.isAdmin, courseController.updateCourseState);

    router.patch("/updateCourseTitle", mdw.isAdmin, courseController.updateCourseTitle);

    router.get("/getAllCoursesByDivision", mdw.isPupil, courseController.getAllCoursesByDivision);

    router.delete("/deleteCourse", mdw.isAdmin, courseController.deleteCourse);

    return router;

}