'use strict';
var Division = require('../../database/models').Division;
var Course = require('../../database/models').Course;
var User = require('../../database/models').User;
var Pupil = require('../../database/models').Pupil;

module.exports = {

    addCourse: function (req, res) {
        if (req.body.title && req.body.description && req.body.position && (req.body.state == 1 || req.body.state == 0) && req.body.divisionId) {
            Course.create({
                title: req.body.title,
                description: req.body.description,
                position: req.body.position,
                state: req.body.state,
                DivisionId: req.body.divisionId
            }).then(function (course) {

                return res.json({
                    success: true,
                    courseId: course.id,
                    msg: 'Course created'
                });

            }).catch(function (err) {

                return res.json({
                    success: false,
                    msg: err.toString()
                });

            });
        } else {

            return res.json({
                success: false,
                msg: "Course Infos are required"
            });
        }
    },

    getAllCourses: function (req, res) {

        Course.findAll({
            order: 'position DESC'
        }).then(function (courses) {

            return res.json({
                success: true,
                courses: courses
            });

        }).catch(function (err) {

            return res.json({
                success: false,
                msg: err.toString()
            });

        });
    },

    getCourseByState: function (req, res) {

        if (req.params.state) {
            Course.findAll({
                where: {
                    state: req.params.state
                }
            }).then(function (courses) {
                return res.json({
                    success: true,
                    courses: courses
                });
            }).catch(function (err) {
                return res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        } else {
            return res.json({
                success: false,
                msg: "State is required"
            });
        }
    },

    getCoursesByDivision: function (req, res) {

        if (req.params.divisionId) {
            Course.findAll({
                where: {
                    DivisionId: req.params.divisionId
                }
            }).then(function (courses) {
                return res.json({
                    success: true,
                    courses: courses
                });
            }).catch(function (err) {
                return res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        } else {
    
            return res.json({
                success: false,
                msg: "Division Id is required"
            });
        }
    },

    getCourseByDivisionState: function (req, res) {},

    updateCourse: function (req, res) {},

    updateCourseState: function (req, res) {},

    updateCourseTitle: function (req, res) {},

    getAllCoursesByDivision: function (req, res) {

        Pupil.findOne({
            where: {
                id: req.PupilId
            },
            include: [{
                model: Division,
                include: {
                    model: Course,
                    where: {
                        state: {
                            $ne: 0
                        }
                    }
                }
                // }, {
                //     model: User
                // }
            }],
            order: "position DESC",

        }).then(function (pupil) {
            return res.json({
                success: true,
                courses: pupil.Division.Courses
            })

        }).catch(function (err) {
            return res.json({
                success: false,
                msg: err.toString()
            })
        });
    },

    deleteCourse: function (req, res) {
        if (req.body.courseId) {

            Course.destroy({
                where: {
                    id: req.body.courseId
                }
            }).then(function () {

                return res.json({
                    success: true,
                    msg: 'Course deleted'
                });

            }).catch(function (err) {

                return res.json({
                    success: false,
                    msg: err.toString()
                });

            });
        } else {

            return res.json({
                success: false,
                msg: "Course id is required"
            });

        }
    }

}