'use strict';

var express = require('express');
var router = express.Router();
var divisionController = require("./divisionController.js");
var mdw = require("./../../middlewares/middleware.js");

module.exports = function () {

    router.post('/addDivision', mdw.isAdmin, divisionController.addDivision);

    router.get('/allDivisions', divisionController.allDivisions);

    router.get('/division/:id', divisionController.division);

    router.put('/updateDivsion', mdw.isAdmin, divisionController.updateDivision);

    router.delete('/deleteDivision', mdw.isAdmin, divisionController.deleteDivision);

    return router;
};