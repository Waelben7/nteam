'use strict';
var Division = require('../../database/models').Division;

module.exports = {

    addDivision: function (req, res) {
        if (req.body.name) {
            Division.create({
                name: req.body.name
            }).then(function (division) {

                return res.json({
                    success: true,
                    divisionId: division.id,
                    msg: 'Division created'
                });

            }).catch(function (err) {

                return res.json({
                    success: false,
                    msg: err.toString()
                });

            });
        } else {
            return res.json({
                success: false,
                msg: "Division name is required"
            });
        }
    },
    
    division: function (req, res) {
        if (req.params.id) {
            Division.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function (division) {
                return res.json({
                    success: true,
                    division: division
                });
            }).catch(function (err) {
                return res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        } else {
            return res.json({
                success: false,
                msg: "Division id is required"
            });
        }
    },

    allDivisions: function (req, res) {

        Division.findAll({
            order: 'name DESC'
        }).then(function (divisions) {

            return res.json({
                success: true,
                divisions: divisions
            });

        }).catch(function (err) {

            return res.json({
                success: false,
                msg: err.toString()
            });

        });
    },
    updateDivision: function (req, res) {

        if (req.body.divisionId && req.body.name) {

            Division.update({
                    name: req.body.name
                }, {
                    where: {
                        id: req.body.divisionId
                    }
                }).then(function () {

                    return res.json({
                        success: true,
                        msg: "Division updated"
                    });
                })
                .catch(function (err) {

                    return res.json({
                        success: false,
                        msg: err.toString()
                    });
                });
        } else {

            return res.json({
                success: false,
                msg: "Division id and name are required"
            });
        }
    },

    deleteDivision: function (req, res) {
        if (req.body.divisionId) {

            Division.destroy({
                where: {
                    id: req.body.divisionId
                }
            }).then(function () {

                return res.json({
                    success: true,
                    msg: 'Division deleted'
                });

            }).catch(function (err) {
                console.log("-------------- here ----------------");
                return res.json({
                    success: false,
                    msg: err.toString()
                });

            });
        } else {

            return res.json({
                success: false,
                msg: "Division id is required"
            });

        }
    }
}