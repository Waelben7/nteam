'use strict';

var express = require('express');
var router = express.Router();
var pupilController = require("./pupilController.js");
var mdw = require("./../../middlewares/middleware.js");

module.exports = function () {

    router.patch('/admin/updateAge', mdw.isAdmin, pupilController.updateAge);
    router.patch('/pupil/updateAge', mdw.isPupil, pupilController.updateAge);


    router.patch('/pupil/updateDivision', mdw.isPupil, pupilController.updateDivision);
    router.patch('/admin/updateDivision', mdw.isAdmin, pupilController.updateDivision);

    router.patch('/pupil/updateName', mdw.isPupil, pupilController.updateName);
    router.patch('/admin/updateName', mdw.isAdmin, pupilController.updateName);

    router.patch('/updatePoints', mdw.isAdmin, pupilController.updatePoints);

    router.put('/pupil/updateProfile', mdw.isPupil, pupilController.updateProfile);
    router.put('/admin/updateProfile', mdw.isAdmin, pupilController.updateProfile);

    router.get('/getPupil/:id', mdw.isAdmin, pupilController.getPupil);
    // router.get('/getPupilName/:token', mdw.isAdmin, pupilController.getPupilName);

    router.get('/allPupils', mdw.isAdmin, pupilController.allPupils);

    router.delete('/deletePupil', mdw.isAdmin, pupilController.deletePupil);

    return router;
};