'use strict';
var Division = require('../../database/models').Division;
var Pupil = require('../../database/models').Pupil;
var User = require('../../database/models').User;

module.exports = {

    updateAge: function (req, res) {
        if (req.body.age) {
            if (req.AdminId) {
                if (req.body.pupilId) {
                    Pupil.update({
                            age: req.body.age
                        }, {
                            where: {
                                id: req.body.pupilId
                            }
                        }).then(function () {

                            return res.json({
                                success: true,
                                msg: "Pupil  Age updated"
                            });
                        })
                        .catch(function (err) {

                            return res.json({
                                success: false,
                                msg: err.toString()
                            });
                        });
                } else {
                    return res.json({
                        success: false,
                        msg: "Pupil Id is required"
                    });
                }
            } else {
                Pupil.update({
                        age: req.body.age
                    }, {
                        where: {
                            id: req.PupilId
                        }
                    }).then(function () {

                        return res.json({
                            success: true,
                            msg: "Your Age is updated"
                        });
                    })
                    .catch(function (err) {

                        return res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });
            }
        } else {

            return res.json({
                success: false,
                msg: "Age is required"
            });
        }
    },

    updatePoints: function (req, res) {

        if (req.body.points && req.body.pupilId) {

            Pupil.update({
                    point: req.body.points
                }, {
                    where: {
                        id: req.body.pupilId
                    }
                }).then(function () {

                    return res.json({
                        success: true,
                        msg: "Points updated"
                    });
                })
                .catch(function (err) {

                    return res.json({
                        success: false,
                        msg: err.toString()
                    });
                });
        } else {

            return res.json({
                success: false,
                msg: "Points and Pupil Id are required"
            });
        }

    },

    updateDivision: function (req, res) {

        if (req.body.divisionId) {
            if (req.AdminId) {
                if (req.body.pupilId) {
                    Pupil.update({
                        DivisionId: req.body.divisionId
                    }, {
                        where: {
                            id: req.body.pupilId
                        }
                    }).then(function (pupil) {

                        return res.json({
                            success: true,
                            msg: "Division updated"
                        });
                    }).catch(function (err) {

                        return res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });
                } else {
                    return res.json({
                        success: false,
                        msg: "Pupil Id is required"
                    });
                }
            } else {

                Pupil.update({
                    DivisionId: req.body.divisionId
                }, {
                    where: {
                        id: req.PupilId
                    }
                }).then(function (pupil) {

                    return res.json({
                        success: true,
                        msg: "Your Division is updated"
                    });
                }).catch(function (err) {

                    return res.json({
                        success: false,
                        msg: err.toString()
                    });
                });
            }
        } else {

            return res.json({
                success: false,
                msg: "Division Id is required"
            });
        }

    },

    updateName: function (req, res) {
        if (req.body.name) {

            if (req.AdminId) {
                if (req.body.pupilId && req.body.userId) {

                    User.update({
                        name: req.body.name
                    }, {
                        where: {
                            id: req.UserId
                        }
                    }).then(function () {

                        return res.json({
                            success: true,
                            msg: "Pupil Name is updated"
                        });
                    }).catch(function (err) {

                        return res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });

                } else {

                    return res.json({
                        success: false,
                        msg: "Pupil Id and user Id are required"
                    });
                }
            } else {

                User.update({
                    name: req.body.name
                }, {
                    where: {
                        id: req.UserId
                    }
                }).then(function () {
                    return res.json({
                        success: true,
                        msg: "Your Name is updated"
                    });
                }).catch(function (err) {

                    return res.json({
                        success: false,
                        msg: err.toString()
                    });
                })
            }
        } else {

            return res.json({
                success: false,
                msg: "New Name is required"
            });
        }
    },

    updateProfile: function (req, res) {
        if (req.body.age && req.body.name && req.body.divisionId) {
            if (req.PupilId) {

                User.update({
                    name: req.body.name
                }, {
                    where: {
                        id: req.UserId
                    }
                }).then(function () {

                    Pupil.update({
                        age: req.body.age,
                        DivisionId: req.body.divisionId
                    }, {
                        where: {
                            id: req.PupilId
                        }
                    }).then(function () {

                        return res.json({
                            success: true,
                            msg: "Your Profile is updated"
                        });
                    }).catch(function (err) {

                        return res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });
                }).catch(function (err) {
                    return res.json({
                        success: false,
                        msg: err.toString()
                    });
                });
            } else {
                if (req.body.points && req.body.pupilId && req.body.userId) {

                    User.update({
                        name: req.body.name
                    }, {
                        where: {
                            id: req.body.userId
                        }
                    }).then(function () {
                        Pupil.update({
                            age: req.body.age,
                            DivisionId: req.body.divisionId,
                            point: req.body.points
                        }, {
                            where: {
                                id: req.body.pupilId
                            }
                        }).then(function () {

                            return res.json({
                                success: true,
                                msg: "Pupil Profile is updated"
                            });
                        }).catch(function (err) {

                            return res.json({
                                success: false,
                                msg: err.toString()
                            });
                        });
                    }).catch(function (err) {
                        return res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });

                } else {
                    return res.json({
                        success: false,
                        msg: "Points,pupil Id and user Id are required"
                    });
                }
            }
        } else {

            return res.json({
                success: false,
                msg: "Profile infos are required"
            });
        }
    },

    getPupil: function (req, res) {
        if (req.params.id) {
            Pupil.findOne({
                where: {
                    id: req.params.id
                }
            }).then(function (pupil) {
                return res.json({
                    success: true,
                    pupil: pupil
                });
            }).catch(function (err) {
                return res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        } else {
            return res.json({
                success: false,
                msg: "Pupil id is required"
            });
        }
    },

    // getPupilName: function (req, res) {
    //     if (req.params.token) {
    //         User.findOne({
    //             where: {
    //                 id: req.params.token
    //             }
    //             // ,
    //             // include: [{
    //             //     model: User
    //             // }]
    //         }).then(function (user) {
                
    //             return res.json({
    //                 success: true,
    //                 name: user.name
    //             });
    //         }).catch(function (err) {
           
    //             return res.json({
    //                 success: false,
    //                 msg: err.toString()
    //             });
    //         });
    //     } else {
    //         return res.json({
    //             success: false,
    //             msg: "Pupil id is required"
    //         });
    //     }
    // },

        allPupils: function (req, res) {
        Pupil.findAll()
            .then(function (pupils) {

                return res.json({
                    success: true,
                    pupils: pupils
                });

            }).catch(function (err) {

                return res.json({
                    success: false,
                    msg: err.toString()
                });

            });
    },

    deletePupil: function (req, res) {
        if (req.body.pupilId) {

            Pupil.destroy({
                where: {
                    id: req.body.pupilId
                }
            }).then(function () {

                return res.json({
                    success: true,
                    msg: 'Pupil deleted'
                });

            }).catch(function (err) {

                return res.json({
                    success: false,
                    msg: err.toString()
                });

            });
        } else {

            return res.json({
                success: false,
                msg: "Pupil id is required"
            });

        }
    }

}