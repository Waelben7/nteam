'use strict';

var express = require('express');
var router = express.Router();
var viewsController = require("./viewsController.js");
var mdw = require("./../../middlewares/middleware.js");

module.exports = function () {

    router.get('/', mdw.isAuthenticated, viewsController.redirection);

    return router;
};