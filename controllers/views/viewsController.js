'use strict';

var db = require('../../database/models')

module.exports = {

    redirection: function (req, res) {

        if (req.user) {

            if (req.user.Pupils.length > 0) {

                res.sendFile('/client/views/pupilApp/pupil.html', {
                    root: __dirname + '/../..'
                });

            } else if (req.user.Admins.length > 0) {

                res.sendFile('/client/views/adminApp/admin.html', {
                    root: __dirname + '/../..'
                });

            } else {
                res.status(403).json({
                    msg: "Forbidden Access "
                });
            }

        } else {

            res.sendFile('/client/views/mainApp/index.html', {
                root: __dirname + '/../..'
            });

        }
    }
}