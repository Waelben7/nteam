'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('Pupils', 'DivisionId', {
      type: Sequelize.INTEGER,
      references: {
        model: "Divisions",
        key: "id"
      },
      onUpdate: "CASCADE",
      onDelete: "SET NULL"
    });
  },

  down: function (queryInterface, Sequelize) {
     return queryInterface.removeColumn('Pupils', 'DivisionId');
  }
};
