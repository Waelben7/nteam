'use strict';
module.exports = {
    up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('Messages', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          from: {
            type: Sequelize.INTEGER,
            references: {
              model: "Users",
              key: "id"
            },
            onUpdate: "CASCADE",
            onDelete: "SET NULL"
          },
        to: {
          type: Sequelize.INTEGER,
          references: {
              model: "Users",
              key: "id"
            },
            onUpdate: "CASCADE",
            onDelete: "SET NULL"
        },
        msg: {
          type: Sequelize.TEXT
        },
        seen: {
          type: Sequelize.BOOLEAN
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        delatedAt: {
          type: Sequelize.DATE
        }
      });
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('Messages');
  }
};