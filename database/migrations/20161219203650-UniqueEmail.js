'use strict';

module.exports = {
  up: 
  function (queryInterface, Sequelize) {
    queryInterface.addIndex('Users', ['email'], {
      indexName: 'Unique Email',
      indicesType: 'UNIQUE'
    });
  }
  ,

  down: function (queryInterface, Sequelize) {
    queryInterface.removeIndex('Users', 'Unique Email');
  }
};