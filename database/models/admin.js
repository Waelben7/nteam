'use strict';
module.exports = function (sequelize, DataTypes) {
  var Admin = sequelize.define('Admin', {
    UserId: DataTypes.INTEGER
  }, {
    paranoid: true,
    classMethods: {
      associate: function (models) {
        Admin.belongsTo(models.User);
      }
    }
  });
  return Admin;
};