'use strict';
module.exports = function(sequelize, DataTypes) {
  var Content = sequelize.define('Content', {
    title: DataTypes.STRING,
    type: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    url: DataTypes.STRING
  }, {
    paranoid: true,
    classMethods: {
      associate: function(models) {
         Content.hasMany(models.CourseContent);
      }
    }
  });
  return Content;
};