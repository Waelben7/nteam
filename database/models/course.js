'use strict';
module.exports = function (sequelize, DataTypes) {
  var Course = sequelize.define('Course', {
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    position: DataTypes.INTEGER,
    state: DataTypes.INTEGER  // 0:non publiee 1: publiee
  }, {
    paranoid: true,
    classMethods: {
      associate: function (models) {
        Course.belongsTo(models.Division);
        Course.hasMany(models.CourseContent);
      }
    }
  });
  return Course;
};