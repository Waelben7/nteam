'use strict';
module.exports = function (sequelize, DataTypes) {
  var CourseContent = sequelize.define('CourseContent', {
    ContentId: DataTypes.INTEGER,
    CourseId: DataTypes.INTEGER
  }, {
    paranoid: true,
    classMethods: {
      associate: function (models) {
        CourseContent.belongsTo(models.Course, {
          foreignKey: {
            unique: 'uniqueCourseContent'
          }
        });
        CourseContent.belongsTo(models.Content, {
          foreignKey: {
            unique: 'uniqueCourseContent'
          }
        });
      }
    }
  });
  return CourseContent;
};