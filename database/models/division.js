'use strict';
module.exports = function (sequelize, DataTypes) {
  var Division = sequelize.define('Division', {
    name: DataTypes.STRING
  }, {
    //paranoid: true,
    classMethods: {
      associate: function (models) {
        Division.hasMany(models.Pupil);
        Division.hasMany(models.Course);
      }
    }
  });
  return Division;
};