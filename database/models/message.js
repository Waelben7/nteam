'use strict';
module.exports = function(sequelize, DataTypes) {
  var Message = sequelize.define('Message', {
    from: DataTypes.INTEGER,
    to: DataTypes.INTEGER,
    msg: DataTypes.TEXT,
    seen: DataTypes.BOOLEAN
  }, {
    paranoid:true,
    classMethods: {
      associate: function(models) {
        Message.belongsTo(models.User,{as: "Sender", foreignKey: "from"});
        Message.belongsTo(models.User,{as: "Receiver", foreignKey: "to"});
      }
    }
  });
  return Message;
};