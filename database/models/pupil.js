'use strict';
module.exports = function(sequelize, DataTypes) {
  var Pupil = sequelize.define('Pupil', {
    age: DataTypes.INTEGER,
    point: DataTypes.INTEGER
  }, {
    paranoid: true,
    classMethods: {
      associate: function(models) {
        Pupil.belongsTo(models.User);
        Pupil.belongsTo(models.Division);
      }
    }
  });
  return Pupil;
};