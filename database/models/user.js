'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING
  }, {
    //paranoid:true,
    classMethods: {
      associate: function(models) {
        User.hasMany(models.Pupil);
        User.hasMany(models.Admin);
      }
    }
  });
  return User;
};