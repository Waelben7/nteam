var Admin = require('../database/models').Admin;
var User = require('../database/models').User;
var Pupil = require('../database/models').Pupil;

module.exports = {

    isAuthenticated: function (req, res, next) {

        if (req.session.token) {

            User.findOne({
                where: {
                    token: req.session.token
                },
                include: [{
                    model: Pupil
                }, {
                    model: Admin
                }]
            }).then(function (user) {

                if (user) {

                    req.user = user
                    return next();

                } else {
                   return next();
                }
                
            }).catch(function (err) {
                res.json({
                    success: false,
                    msg: err.toString()
                });
            });

        } else {
           return next();
        }
    },

    isAdmin: function (req, res, next) {
        if (req.headers.token) {

            User.findOne({
                where: {
                    token: req.headers.token
                },
                attributes: ['id']
            }).then(function (user) {
                if (user) {
                    Admin.findOne({
                        where: {
                            UserId: user.id
                        }
                    }).then(function (admin) {
                        if (admin) {
                            req.UserId = user.id
                            req.AdminId = admin.id
                            return next();
                        } else
                            res.json({
                                success: false,
                                msg: "You don't have permission"
                            });
                    }).catch(function (err) {
                        res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });
                } else {
                    res.json({
                        success: false,
                        msg: "You should login first!!"
                    });
                }
            }).catch(function (err) {
                res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        } else {
            res.json({
                success: false,
                msg: 'Token necessary'
            });
        }
    },

    isPupil: function (req, res, next) {
        if (req.headers.token) {
            User.findOne({
                where: {
                    token: req.headers.token
                },
                attributes: ['id']
            }).then(function (user) {
                if (user) {
                    Pupil.findOne({
                        where: {
                            UserId: user.id
                        }
                    }).then(function (pupil) {
                        if (pupil) {
                            req.UserId = user.id
                            req.PupilId = pupil.id
                            req.pupilDiv = pupil.DivisionId
                            return next();
                        } else {
                            res.json({
                                success: false,
                                msg: "You don't have permission"
                            });
                        }
                    }).catch(function (err) {
                        res.json({
                            success: false,
                            msg: err.toString()
                        });
                    });
                } else {
                    res.json({
                        success: false,
                        msg: "You should login first!!"
                    });
                }
            }).catch(function (err) {
                res.json({
                    success: false,
                    msg: err.toString()
                });
            });
        } else {
            res.json({
                success: false,
                msg: 'Token necessary'
            });
        }
    }

};