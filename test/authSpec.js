var expect = require("chai").expect;
var request = require("supertest");
var app = require("../app.js");
var token1;
var db = require('../database/models');


describe("Auth Module", function () {

    it("Should register", function (done) {
        var user = {
            'email': 'test@mail.node',
            'password': '123',
            'name': 'Ghada',
            'age': 18,
            'divisionId': 4
        };

        request(app).post('/api/register')
            .send(user).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                expect(object.token).to.exist;
                token1 = {
                    token: object.token
                }
                done();
            });
    });

    it("Should logout", function (done) {

        request(app).post('/api/logout')
            .send(token1).expect(200)
            .end(function (err, res) {
                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });
    });

    it("Should login", function (done) {
        var user = {
            'email': 'test@mail.node',
            'password': '123'
        };

        request(app).post('/api/login')
            .send(user).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                expect(object.token).to.exist;
                token1 = {
                    token: object.token
                }
                done();
            });
    });

    it("Should logout", function (done) {

        request(app).post('/api/logout')
            .send(token1).expect(200)
            .end(function (err, res) {
                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });
    });

    it("should destroy", function (done) {

        db.User.findOne({
            where: {
                email: "test@mail.node"
            }
        }).then(function (user) {

            db.Pupil.destroy({
                where: {
                    UserId: user.id
                }
            }).then(function (affRows) {

                db.User.destroy({
                    where: {
                        id: user.id
                    }
                }).then(function (affRows) {
                    expect(affRows).to.equal(1);
                }).catch(function (err) {
                    expect(err).to.be.null;
                })
                expect(affRows).to.equal(1);
                done();
            }).catch(function (err) {

                expect(err).to.be.null;
                done();
            });
        }).catch(function () {

            expect(err).to.be.null;
            done();
        });

    });

});