var expect = require("chai").expect;
var request = require("supertest");
var app = require("../app.js");
var tokenPupil;
var tokenAdmin;
var usersManip = require("../common/generate&destroyTokens.js");
var courseId;
var divisionId;

describe("Course Module", function () {

    this.timeout(5000);

    before(function (done) {
        usersManip.generateTokenForPupil(function (res) {

            tokenPupil = res.token;
            divisionId = res.divisionId;
            usersManip.generateTokenForAdmin(function (res) {
                tokenAdmin = res.token

                done();
            })
        })
    });

    it("Should Add Course : Admin", function (done) {

        var course = {
            'title': "Electrostatique",
            'description': "physique",
            'position': 1,
            'state': 0,
            'divisionId': 1
        };

        request(app).post('/api/course/addCourse')
            .send(course).expect(200)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                courseId = object.courseId;
                done();
            });
    })

    it("Should Get All Courses : Admin", function (done) {

        request(app).get('/api/course/getAllCourses')
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                //console.log(object.courses);
                expect(object.success).to.equal(true);

                done();
            });
    })

    it("Should Get Course By State : Admin", function(done){

        request(app).get('/api/course/getCourseByState/' + 1)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                //console.log(object.courses);
                expect(object.success).to.equal(true);

                done();
            });

    })

    it("Should Get Courses By Division : Admin", function(done){

        console.log(divisionId);

        request(app).get('/api/course/getCoursesByDivision/' + divisionId)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                console.log(object.courses);
                expect(object.success).to.equal(true);

                done();
            });
        
    })

    // it("Should Update Course : Admin", function(done){

    // })

    it("Should Get All Courses By Division : Pupil", function(done){

        request(app).get('/api/course/getAllCoursesByDivision')
            .set('token', tokenPupil)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                // console.log(object.pupil);
                expect(object.success).to.equal(true);

                done();
            });

    })

    // it("should Get Course By Division And State : Admin ", function(done){

    // })

    // it("Should Update Course State : Admin", function(done){

    // })

    // it("Should Update Course Title : Admin", function(done){

    // })

    it("Should Delete Course : Admin", function (done) {

        var course = {
            'courseId': courseId
        };

        request(app).delete('/api/course/deleteCourse')
            .set('token', tokenAdmin)
            .send(course).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);

                done();
            });
    })

    after(function (done) {

        usersManip.destroyTokens(tokenPupil, tokenAdmin, function (res) {
            done();
        });
    });

});