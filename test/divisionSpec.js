var expect = require("chai").expect;
var request = require("supertest");
var app = require("../app.js");
var usersManip = require("../common/generate&destroyTokens.js");
var divisionId ;

describe("Division Module", function () {

    this.timeout(5000);

    before(function (done) {
        usersManip.generateTokenForPupil(function (res) {

            tokenPupil = res.token;
            // pupilId = res.pupilId;
            // userId = res.userId;
            usersManip.generateTokenForAdmin(function (res) {
                tokenAdmin = res.token

                done();
            })
        })
    });

    it("Should Add Division : Admin", function (done) {

        var division = {
            'name': 'Economie'
        };
        request(app).post('/api/division/addDivision')
            .send(division).expect(200)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                divisionId = object.divisionId;
                done();
            });
    })

    it("Should Update Division : Admin", function (done) {

        var division = {
            'name': 'Economie & Gestion',
            'divisionId': divisionId
        };
   
        request(app).put('/api/division/updateDivsion')
            .send(division).expect(200)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);

                done();
            });
    })

    it("Should Get All Divisions", function (done) {

        request(app).get('/api/division/allDivisions')
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                //console.log(object.divisions);
                expect(object.success).to.equal(true);
                
                done();
            });
    })

    it("Should Get Division", function (done) {

        request(app).get('/api/division/division/'+divisionId)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                //console.log(object.division);
                expect(object.success).to.equal(true);
                
                done();
            });
    })

    it("Should Delete Division : Admin", function (done) {

        var division = {
            'divisionId': divisionId
        };

        request(app).delete('/api/division/deleteDivision')
            .set('token', tokenAdmin)
            .send(division).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                
                done();
            });
    })


    after(function (done) {

        usersManip.destroyTokens(tokenPupil, tokenAdmin, function (res) {
            done();
        });
    });

});