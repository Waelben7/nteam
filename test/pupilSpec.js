var expect = require("chai").expect;
var request = require("supertest");
var app = require("../app.js");
var tokenPupil;
var tokenAdmin;
var usersManip = require("../common/generate&destroyTokens.js");
var pupilId;
var userId;


describe("Pupil Module", function () {

    this.timeout(5000);

    before(function (done) {
        usersManip.generateTokenForPupil(function (res) {

            tokenPupil = res.token;
            pupilId = res.pupilId;
            userId = res.userId;
            usersManip.generateTokenForAdmin(function (res) {
                tokenAdmin = res.token

                done();
            })
        })
    });

    it("Should Update Profile As Pupil", function (done) {

        var pupil = {
            'age': 15,
            'name': 'pupil',
            'divisionId': 1
        };

        request(app).put('/api/pupil/pupil/updateProfile')
            .send(pupil).expect(200)
            .set('token', tokenPupil)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);

                done();
            });
    })

    it("Should Update Profile As Admin", function (done) {

        var pupil = {
            'age': 10,
            'name': 'pupil',
            'divisionId': 4,
            'points': 200,
            'pupilId': pupilId,
            'userId': userId
        };

        request(app).put('/api/pupil/admin/updateProfile')
            .send(pupil).expect(200)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);

                done();
            });
    })

    it("Should Get Pupil Infos : Admin", function (done) {

        request(app).get('/api/pupil/getPupil/' + pupilId)
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                //console.log(object.pupil);
                done();
            });

    })

    // it("Should Get Pupil Name : Admin", function (done) {

    //     request(app).get('/api/pupil/getPupilName/' + tokenPupil)
    //         .set('token', tokenAdmin)
    //         .end(function (err, res) {

    //             expect(err).to.be.null;
    //             var object = JSON.parse(res.text);
    //             expect(object.success).to.equal(true);
    //             console.log(object.name);
    //             done();
    //         });

    // })

    it("Should Get All Pupils Infos : Admin", function (done) {

        request(app).get('/api/pupil/allPupils')
            .set('token', tokenAdmin)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
               // console.log(object.pupils);
                done();
            });

    })

    it("Should Update Name : Pupil", function (done) {

        var pupil = {
            'name': "new name"
        };

        request(app).patch('/api/pupil/pupil/updateName')
            .set('token', tokenPupil)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Update Division : Pupil", function (done) {

        var pupil = {
            'divisionId': 4
        };

        request(app).patch('/api/pupil/pupil/updateDivision')
            .set('token', tokenPupil)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Update Division : Admin", function (done) {

        var pupil = {
            'divisionId': 4,
            'pupilId': pupilId
        };

        request(app).patch('/api/pupil/admin/updateDivision')
            .set('token', tokenAdmin)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Update Age : Pupil", function (done) {

        var pupil = {
            'age': 17
        };

        request(app).patch('/api/pupil/pupil/updateAge')
            .set('token', tokenPupil)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Update Age : Admin", function (done) {

        var pupil = {
            'age': 17,
            'pupilId': pupilId
        };

        request(app).patch('/api/pupil/admin/updateAge')
            .set('token', tokenAdmin)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Delete Pupil : Admin", function (done) {

        var pupil = {
            'pupilId': pupilId
        };

        request(app).delete('/api/pupil/deletePupil')
            .set('token', tokenAdmin)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Update Points : Admin", function (done) {

        var pupil = {
            'pupilId': pupilId,
            'points': 150
        };

        request(app).patch('/api/pupil/updatePoints')
            .set('token', tokenAdmin)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })

    it("Should Update Name : Admin", function (done) {

        var pupil = {
            'pupilId': pupilId,
            'name': "new name",
            'userId': userId
        };

        request(app).patch('/api/pupil/admin/updateName')
            .set('token', tokenAdmin)
            .send(pupil).expect(200)
            .end(function (err, res) {

                expect(err).to.be.null;
                var object = JSON.parse(res.text);
                expect(object.success).to.equal(true);
                done();
            });

    })


    after(function (done) {

        usersManip.destroyTokens(tokenPupil, tokenAdmin, function (res) {
            done();
        });
    });

});